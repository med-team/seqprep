<p>SeqPrep is a program to merge paired end Illumina reads that are overlapping into a single longer read. It may also just be used for its adapter trimming feature without doing any paired end overlap. When an adapter sequence is present, that means that the two reads must overlap (in most cases) so they are forcefully merged. When reads do not have adapter sequence they must be treated with care when doing the merging, so a much more specific approach is taken. The default parameters were chosen with specificity in mind, so that they could be ran on libraries where very few reads are expected to overlap. It is always safest though to save the overlapping procedure for libraries where you have some prior knowledge that a significant portion of the reads will have some overlap. </p>

<p>Before running SeqPrep make sure to check that the program's defaults are indeed the adapters you are looking for. Try copying the default forward adapter from this file and grep it against your reads doing a word count, also try the same with the reverse adapter with grep. You should see some hits. You can also try using (and validating with grep) <code>-A GATCGGAAGAGCACACG -B AGATCGGAAGAGCGTCGT</code> as parameters. To find a list of Illumina adapter sequences you should write to Illumina tech support TechSupport@illumina.com (they do not like people to share the list of sequences outside of their institution).</p>

<p>Chose about 20bp of an adapter sequence where:</p>

<ol>
<li>You see the most hits with grep</li>
<li>When you run a command like <code>zcat Lane2_0d_2.fastq.gz | head -n 1000000 |grep "INSERT ADAPTER HERE" | head</code> you see the adapter sequence show up at the beginning of a few reads. Also the -A and -B arguments should be as they show up in your data, SeqPrep searches directly for these sequences without doing reverse complementing.</li>
<li>Check the forward and reverse and make sure that you have roughly the same number of hits via a command to count hits like: <code>zcat Lane2_0d_2.fastq.gz | head -n 1000000 |grep "INSERT ADAPTER HERE" | wc -l</code></li>
</ol>

<p>As an additional precaution, the program checks for good read overlap once the adapters are trimmed. If the adapter is trimmed and the reads do not have a reasonable adapter overlap (you can modify this setting with -X) then the reads aren't printed or merged. </p>

<p>See <code>Test/README.md</code> for some information on testing out other parameters. <code>Test/SimTest</code> has some particularly cool test data which you can use to check out sensitivity and specificity of adapter trimming using different parameters. The results of the test are displayed in <code>results.html</code> which uses the google charts API so that the points are interactive and you can easily determine which settings made which points.</p>

<p>Usage:</p>

<pre><code>./SeqPrep [Required Args] [Options]
NOTE 1: The output is always gziped compressed.
NOTE 2: If the quality strings in the output contain characters less than ascii 33 on an ascii table (they look like lines from a binary file), try running again with or without the -6 option.
</code></pre>

<p>Required Arguments:</p>

<pre><code>-f &lt;first read input fastq filename&gt;
-r &lt;second read input fastq filename&gt;
-1 &lt;first read output fastq filename&gt;
-2 &lt;second read output fastq filename&gt;
</code></pre>

<p>General Arguments (Optional):</p>

<pre><code>-3 &lt;first read discarded fastq filename&gt;
-4 &lt;second read discarded fastq filename&gt;
-h Display this help message and exit (also works with no args) 
-6 Input sequence is in phred+64 rather than phred+33 format, the output will still be phred+33 
-q &lt;Quality score cutoff for mismatches to be counted in overlap; default = 13&gt;
-L &lt;Minimum length of a trimmed or merged read to print it; default = 30&gt;
</code></pre>

<p>Arguments for Adapter/Primer Trimming (Optional):</p>

<pre><code>-A &lt;forward read primer/adapter sequence to trim as it would appear at the end of a read (recommend about 20bp of this)
     (should validate by grepping a file); default (genomic non-multiplexed adapter1) = AGATCGGAAGAGCGGTTCAG&gt;
-B &lt;reverse read primer/adapter sequence to trim as it would appear at the end of a read (recommend about 20bp of this)
     (should validate by grepping a file); default (genomic non-multiplexed adapter2) = AGATCGGAAGAGCGTCGTGT&gt;
-O &lt;minimum overall base pair overlap with adapter sequence to trim; default = 10&gt;
-M &lt;maximum fraction of good quality mismatching bases for primer/adapter overlap; default = 0.020000&gt;
-N &lt;minimum fraction of matching bases for primer/adapter overlap; default = 0.870000&gt;
-b &lt;adapter alignment band-width; default = 50&gt;
-Q &lt;adapter alignment gap-open; default = 8&gt;
-t &lt;adapter alignment gap-extension; default = 2&gt;
-e &lt;adapter alignment gap-end; default = 2&gt;
-Z &lt;adapter alignment minimum local alignment score cutoff [roughly (2*num_hits) - (num_gaps*gap_open) - (num_gaps*gap_close) - (gap_len*gap_extend) - (2*num_mismatches)]; default = 26&gt;
-w &lt;read alignment band-width; default = 50&gt;
-W &lt;read alignment gap-open; default = 26&gt;
-p &lt;read alignment gap-extension; default = 9&gt;
-P &lt;read alignment gap-end; default = 5&gt;
-X &lt;read alignment maximum fraction gap cutoff; default = 0.125000&gt;
-z &lt;use mask; N will replace adapters&gt;
</code></pre>

<p>Optional Arguments for Merging:</p>

<pre><code>-y &lt;maximum quality score in output ((phred 33) default = ']' )&gt;
-g &lt;print overhang when adapters are present and stripped (use this if reads are different length)&gt;
-s &lt;perform merging and output the merged reads to this file&gt;
-E &lt;write pretty alignments to this file for visual Examination&gt;
-x &lt;max number of pretty alignments to write (if -E provided); default = 10000&gt;
-o &lt;minimum overall base pair overlap to merge two reads; default = 15&gt;
-m &lt;maximum fraction of good quality mismatching bases to overlap reads; default = 0.020000&gt;
-n &lt;minimum fraction of matching bases to overlap reads; default = 0.900000&gt;
</code></pre>

<p>My current strategy to deal with ambiguous alignments to low complexity regions is as follows:</p>

<ol>
<li>I have some minimum requirements for an overlap to be accepted</li>
<li>After the first one is found (ie the one with the maximal overlap between the two sequences), if low complexity filtering is enabled, I keep searching</li>
<li>if a second viable hit is found, I give up and say that it is not a good idea to merge the two reads.</li>
</ol>

<p>I check for ambiguous alignments in read overlapping, but not in adapter trimming where the most conservative thing to do is strip the most aggressively aligned adapter (The closest to the beginning of the read).</p>

<p>To accept an alignment I allow some fraction of mismatches (currently the floor of 0.06 of the alignment length for adapter and 0.02 of the alignment length for two reads). That means that in most cases for overlapping two reads I don't allow any mismatches between adjacent reads, but if there is a 50bp potential overlap with 1 mismatch over q20 for example, I allow it. Anything below 50 needs to be perfect other than with low quality bases.</p>

<p>Since we ignore poor quality bases, we could have the case where a single real match followed by a long string of poor quality bases to the end of the read would result in a called overlap. That seemed like a bad idea. To get around that I require that at least some fraction of the overlapping length be matches. Right now I have that parameter set at 0.7 for adapter trimming and 0.75 for read merging, so for a case where only the last 10 bases overlap, at least 7 of those must be matches. </p>

<p>Since doing that many floating point multiplications seems like a bad idea, I just have a table that pre-calculates all of those min matches and max mismatch numbers for every overlap length up to the maximum allowed read length.</p>

<p>Finally I have a parameter you can set which specifies a minimum resulting read length after adapter trimming and/or merging so that ultra short trimmed reads aren't output.</p>

<p>Following are results from hand testing the three main merge cases. Now to generate similar output automatically just supply the <code>-E readable_alignment.txt.gz</code> argument to the program (the output is gzip compressed into the file name specified). </p>

<p>Sequence Merge No Adapter Present:</p>

<pre><code>QUER: NCCTGCTACTACCACCCGTTCCGTGCCTGGAGCCTGCATGTTGGGCAGATACGTGCTGCCACAGCCTGTCTCTGCTGGTGCCTGGGCCTC
                                        ||  |||||||||||| || |  |||||||||||||||||||||||||||||||||
SUBJ:                                   TGTGTGTTGGGCAGATGCGGGGGGCCACAGCCTGTCTCTGCTGGTGCCTGGGCCTCTCCTGTTCCTTGCCCACGTCTCCGTCTCCTGTTG
RESU: NCCTGCTACTACCACCCGTTCCGTGCCTGGAGCCTGCATGTTGGGCAGATACGTGCTGCCACAGCCTGTCTCTGCTGGTGCCTGGGCCTCTCCTGTTCCTTGCCCACGTCTCCGTCTCCTGTTG
Quality Merge:
QUER: !223387787@@@CCC22C@@@@@@@@@@@@@@@@@@@@@@@@@@@@?@@89887:::::.2125@@:@@:::::@@@@@&lt;&lt;::8@@@@@
SUBJ:                                   !!!!!!!!!!!!!!!!!!!!!!!!!!!@@@8DEGE@EDDBB2&lt;BBE@EHBFE@EE&gt;D8@DBE&gt;BFIDH@IIEEIIBEIEIIGBIIGIFII
RESU: !223387787@@@CCC22C@@@@@@@@@@@@@@@@@@@@@@@@@@@@?@@89887:::::.QPQLSSSSSSSSSSQSSSSSSSSSSSSSSD8@DBE&gt;BFIDH@IIEEIIBEIEIIGBIIGIFII
</code></pre>

<p>Sequence Merge Adapter Present, Easy Peezy Mode (same lengths):</p>

<pre><code>SUBJ: NGATATGATTCCCAATCTAAGCAAACTGTCATGGAAAC
       |||||||||||||||||||||||||||||||||||||
QUER: GGATATGATTCCCAATCTAAGCAAACTGTCATGGAAAC
RESU: GGATATGATTCCCAATCTAAGCAAACTGTCATGGAAAC
Quality Merge:
SUBJ: !.-/.53444@@@@@@@@@@@@@@@@@@@@@@@@@@@@
QUER: IHGIIIDIIHGEHIGHIFHIFIIIIHIIIIIIIIIHII
RESU: ISSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
</code></pre>

<p>Sequence merge Adapter but lengths differ:</p>

<pre><code>SUBJ: AATTGATGGGTGCCCACCCACGGGCCAGACAAAATCATCTGGCAAGCTGGATGCAGCCTACAAGCTGTAAGATTGGA
      |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
QUER: AATTGATGGGTGCCCACCCACGGGCCAGACAAAATCATCTGGCAAGCTGGATGCAGCCTACAAGCTGTA
RESU: AATTGATGGGTGCCCACCCACGGGCCAGACAAAATCATCTGGCAAGCTGGATGCAGCCTACAAGCTGTAAGATTGGA
Quality Merge:
SUBJ: =DEC??DDBD?4B=BEE@@@GB&gt;GEE:DE8=2::6GDGBGEGDD&lt;=;A?=AGGGG=5.=&lt;BD?B?DDB&gt;B4725:E&gt;
QUER: GDDBBFBGGFBHFIEDGGGBDGGG&lt;GGDDG@IIIEIHDIHGIIIDDGDGDFDIFIHGIDEGGGDIIIGI
RESU: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSB4725:E&gt;
</code></pre>

<p>If interested there is a website where I post my tests of different parameters for SeqPrep on simulated data. There are also a few comparison stats of different programs to trim adapters. The website can be accessed here:
http://hgwdev.cse.ucsc.edu/~jstjohn/seqprep/</p>

<p>where the pages are named <code>result(date).html</code>. The latest ones (as of when I have gotten around to edit this) can be found here:</p>

<p>http://hgwdev.cse.ucsc.edu/~jstjohn/seqprep/results2011-09-15.html</p>

<p>Note that although my program is more sensitive and specific than fastq-clipper, I optimized my default parameters based on this test. Results on real data may be
different, although I believe my method takes advantage of a more realistic adapter model than other software does. For example, even though my program requires
10bp of adapter to be present at the end of a read to trim it off (by default) there is a backup adapter trimming function that trimms based on strong and
unambiguous read overlap. Because of this my program can trim the adapter even if it is only present in the last few bases of the read.</p>

<p>Also note that fastq-mcf appears to do a little better at sensitivity (0.992 vs 0.985) at a very large cost to specificity (0.497 vs 0.994).</p>
